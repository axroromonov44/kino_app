import '../resources/resources.dart';

class Movie {
  final int id;
  final String imageName;
  final String title;
  final String time;
  final String description;

  Movie({
    required this.id,
    required this.imageName,
    required this.title,
    required this.time,
    required this.description,
  });
  final _movies = [
    Movie(
      id: 1,
      imageName: AppImages.movie,
      title: 'Смертельная битва',
      time: 'April 7, 2021',
      description:
      'Wkghorr trhko olohhkot ookhtokho hrehhr hrhhrhr hrhrhrh rgrhh',
    ),
    Movie(
      id: 2,
      imageName: AppImages.movie,
      title: 'Прибытие',
      time: 'April 7, 2021',
      description:
      'Wkghorr trhko olohhkot ookhtokho hrehhr hrhhrhr hrhrhrh rgrhh',
    ),
    Movie(
      id: 3,
      imageName: AppImages.movie,
      title: 'Назад в будующие 1',
      time: 'April 7, 2021',
      description:
      'Wkghorr trhko olohhkot ookhtokho hrehhr hrhhrhr hrhrhrh rgrhh',
    ),
    Movie(
      id: 4,
      imageName: AppImages.movie,
      title: 'Назад в будующие 1',
      time: 'April 7, 2021',
      description:
      'Wkghorr trhko olohhkot ookhtokho hrehhr hrhhrhr hrhrhrh rgrhh',
    ),
    Movie(
      id: 5,
      imageName: AppImages.movie,
      title: 'Назад в будующие 3',
      time: 'April 7, 2021',
      description:
      'Wkghorr trhko olohhkot ookhtokho hrehhr hrhhrhr hrhrhrh rgrhh',
    ),
    Movie(
      id: 6,
      imageName: AppImages.movie,
      title: 'Первому игроку приготовится',
      time: 'April 7, 2021',
      description:
      'Wkghorr trhko olohhkot ookhtokho hrehhr hrhhrhr hrhrhrh rgrhh',
    ),
    Movie(
      id: 7,
      imageName: AppImages.movie,
      title: 'Пиксели',
      time: 'April 7, 2021',
      description:
      'Wkghorr trhko olohhkot ookhtokho hrehhr hrhhrhr hrhrhrh rgrhh',
    ),
    Movie(
      id: 8,
      imageName: AppImages.movie,
      title: 'Человек паук',
      time: 'April 7, 2021',
      description:
      'Wkghorr trhko olohhkot ookhtokho hrehhr hrhhrhr hrhrhrh rgrhh',
    ),
    Movie(
      id: 9,
      imageName: AppImages.movie,
      title: 'Лига справедливости',
      time: 'April 7, 2021',
      description:
      'Wkghorr trhko olohhkot ookhtokho hrehhr hrhhrhr hrhrhrh rgrhh',
    ),
    Movie(
      id: 10,
      imageName: AppImages.movie,
      title: 'Джентельмены',
      time: 'April 7, 2021',
      description:
      'Wkghorr trhko olohhkot ookhtokho hrehhr hrhhrhr hrhrhrh rgrhh',
    ),
    Movie(
      id: 11,
      imageName: AppImages.movie,
      title: 'Мстители',
      time: 'April 7, 2021',
      description:
      'Wkghorr trhko olohhkot ookhtokho hrehhr hrhhrhr hrhrhrh rgrhh',
    ),
    Movie(
      id: 12,
      imageName: AppImages.movie,
      title: 'Форд против феррари',
      time: 'April 7, 2021',
      description:
      'Wkghorr trhko olohhkot ookhtokho hrehhr hrhhrhr hrhrhrh rgrhh',
    ),
    Movie(
      id: 13,
      imageName: AppImages.movie,
      title: 'Человек из стали',
      time: 'April 7, 2021',
      description:
      'Wkghorr trhko olohhkot ookhtokho hrehhr hrhhrhr hrhrhrh rgrhh',
    ),
    Movie(
      id: 14,
      imageName: AppImages.movie,
      title: 'Тихие зори',
      time: 'April 7, 2021',
      description:
      'Wkghorr trhko olohhkot ookhtokho hrehhr hrhhrhr hrhrhrh rgrhh',
    ),
    Movie(
      id: 15,
      imageName: AppImages.movie,
      title: 'В бой идут одни старики',
      time: 'April 7, 2021',
      description:
      'Wkghorr trhko olohhkot ookhtokho hrehhr hrhhrhr hrhrhrh rgrhh',
    ),
    Movie(
      id: 16,
      imageName: AppImages.movie,
      title: 'Дюна',
      time: 'April 7, 2021',
      description:
      'Wkghorr trhko olohhkot ookhtokho hrehhr hrhhrhr hrhrhrh rgrhh',
    ),
  ];
}
