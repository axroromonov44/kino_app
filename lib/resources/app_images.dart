part of 'resources.dart';

class AppImages {
  AppImages._();

  static const String movie = 'images/movie.jpg';
  static const String actor = 'images/actor.jpeg';
  static const String topHederSubImage = 'images/top_haeder_sub_image.jpeg';
  static const String topHeader = 'images/top_header.jpeg';
}
